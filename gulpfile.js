'use strict';

var gulp           = require('gulp');
var browserSync    = require('browser-sync').create();
var scss           = require('gulp-sass');
var sourcemaps     = require('gulp-sourcemaps');
var nunjucks       = require('gulp-nunjucks');
var nunjucksRender = require('gulp-nunjucks-render');
var data           = require('gulp-data');
var autoprefixer   = require('gulp-autoprefixer');
var cssnano        = require('gulp-cssnano');
var babel          = require('gulp-babel');
var concat         = require('gulp-concat');
var uglify         = require('gulp-uglify');
var rename         = require('gulp-rename');
var clean          = require('gulp-clean');
var imagemin       = require('gulp-imagemin');
var runSequence = require('run-sequence');

// Static Server + watching scss/html files
gulp.task('serve', ['scss'], function() {

    browserSync.init({
        server: "./dist"
    });

    gulp.watch("src/scss/**/*.scss", ['scss']);
    gulp.watch("src/js/*.js", ['js']);
    gulp.watch("src/asset/**/*", ['images']).on('change', browserSync.reload);
    gulp.watch("src/view/**/*.html", ['html']).on('change', browserSync.reload);
});

gulp.task('clean-css', function () {
    return gulp.src('dist/style/*.css', {read: false})
        .pipe(clean());
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('scss', ['clean-css'], function() {
    return gulp.src("src/scss/*.scss")
        .pipe(sourcemaps.init())
        .pipe(scss().on('error', scss.logError))
        .pipe(autoprefixer())
        .pipe(cssnano())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./dist/style'))
        .pipe(browserSync.stream());
});


gulp.task('clean-html', function () {
    return gulp.src('dist/**/*.html', {read: false})
        .pipe(clean());
});

//Create html files from templates
gulp.task('html', ['clean-html'], function () {

    return gulp.src('src/view/page/**/*.html')
        .pipe(data(function (file) {
            return require('./src/data.json')
        }))
        .pipe(nunjucksRender({
            path: ['src/view/'] // String or Array
        }))
        .pipe(gulp.dest('dist'));
});

gulp.task('clean-js', function () {
    return gulp.src('dist/js/*', {read: false})
        .pipe(clean());
});

// Compile js and concat into main file
gulp.task('js', () =>
    gulp.src('src/js/*.js')
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/js'))
);

gulp.task('externaljs', () =>
    gulp.src('src/external.js')
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(concat('external.js'))
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/js'))
);

// Compile vendor js and concat into vendor file
gulp.task('vendor_js', () =>
    gulp.src(['node_modules/jquery/dist/jquery.js', 'node_modules/js-cookie/src/js.cookie.js'])
        .pipe(sourcemaps.init())
        .pipe(babel({
            presets: ['env']
        }))
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/js'))
);

gulp.task('clean-images', function () {
    return gulp.src('dist/assets', {read: false})
        .pipe(clean());
});


// Compress and move images
gulp.task('images', ['clean-images'], function () {
    gulp.src('src/assets/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/assets'))
});

gulp.task('default', runSequence(['html', 'scss', 'vendor_js', 'js', 'externaljs', 'images'], 'serve'));
